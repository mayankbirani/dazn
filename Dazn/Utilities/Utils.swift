//
//  Utils.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

let scoreReloadTimeInSeconds: Double = 30.0

let genericErrorTitle: String = "Error!"
let genericErrorMessage: String = "Something went wrong, Please try again later!"

func addGradient(_ view: UIView, _ colors: [CGColor]) {
    let layer = CAGradientLayer()
    layer.frame = view.bounds
    layer.colors = colors
    view.layer.insertSublayer(layer, at: 0)
}

func presentErrorAlert(message: String, view: UIViewController) {
    let alert = UIAlertController(title: genericErrorTitle, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(action)
    view.present(alert, animated: true, completion: nil)
}

func getViewController(storyboardName: String, withIdentifier identifier: String) -> UIViewController {
    return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: identifier)
}
