//
//  Colors.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

extension UIColor {
    class var grayGradientMaxColor: UIColor {
        return UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
    }
    class var grayGradientMinColor: UIColor {
        return UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0)
    }
    class var grayCellBackgroundColor: UIColor {
        return UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
    }
    class var blueAppColor: UIColor {
        return UIColor(red: 35/255, green: 78/255, blue: 123/255, alpha: 1.0)
    }
}
