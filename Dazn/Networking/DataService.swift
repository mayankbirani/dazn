//
//  DataService.swift
//  Dazn
//
//  Created by Mayank Birani on 21/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation
import Alamofire

class DataService {
    
    static func getNewsData(request: RequestRouter, successHandler: @escaping SuccessNewsHandler, errorHandler: @escaping ErrorHandler) {
        getData(request: request, alamofireCompletionHandler: { (data) in
            let newsList = Parser.parseNewsData(data: data)
            successHandler(newsList)
        }) { (error) in
            errorHandler(error)
        }
    }
    
    static func getStandingsData(request: RequestRouter, successHandler: @escaping SuccessStandingHandler, errorHandler: @escaping ErrorHandler) {
        getData(request: request, alamofireCompletionHandler: { (data) in
            let standings = Parser.parseStandingData(data: data)
            successHandler(standings)
        }) { (error) in
            errorHandler(error)
        }
    }
    
    static func getScoreData(request: RequestRouter, successHandler: @escaping SuccessScoreHandler, errorHandler: @escaping ErrorHandler) {
        getData(request: request, alamofireCompletionHandler: { (data) in
            let scores = Parser.parseScoreData(data: data)
            successHandler(scores)
        }) { (error) in
            errorHandler(error)
        }
    }
    
    static fileprivate func getData(request: RequestRouter, alamofireCompletionHandler: @escaping AlamofireCompletionHandler, errorHandler: @escaping ErrorHandler) {
        let requestValues = request.values()
        Alamofire.request(requestValues.url, method: requestValues.httpMethod, parameters: requestValues.parameters, headers: nil).response { (response) in
            if let error = response.error {
                errorHandler(error.localizedDescription)
            } else if let data = response.data {
                alamofireCompletionHandler(data)
            }
        }
    }
}

typealias AlamofireCompletionHandler = (_ result: Data) -> Void
typealias SuccessNewsHandler = (_ result: [News]) -> Void
typealias SuccessStandingHandler = (_ result: [Standing]) -> Void
typealias SuccessScoreHandler = (_ result: [Score]) -> Void
typealias ErrorHandler = (_ error: String) -> Void
