//
//  Parser.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation
import SwiftyXMLParser

class Parser {
    
    static func parseNewsData(data: Data) -> [News] {
        let xml = XML.parse(data)
        var newsList: [News] = []
        for hit in xml.rss.channel.item {
            let title = hit.title.text
            let description = hit["description"].text
            let pubDate = hit.pubDate.text
            let link = hit.link.text
            let imageUrl = hit.enclosure.attributes["url"]
            
            let news = News(title: title, description: description, pubDate: pubDate, imageUrl: imageUrl, itemLink: link)
            newsList.append(news)
        }
        return newsList
    }
    
    static func parseScoreData(data: Data) -> [Score] {
        let xml = XML.parse(data)
        var scores: [Score] = []
        for group in xml.gsmrs.competition.season.round.group {
            for match in group.match {
                let date = match.attributes["date_utc"]
                let teamNameA = match.attributes["team_A_name"]
                let teamNameB = match.attributes["team_B_name"]
                let teamScoreA = match.attributes["fs_A"]
                let teamScoreB = match.attributes["fs_B"]
                let score = Score.init(date: date, teamNameA: teamNameA, teamNameB: teamNameB, teamScoreA: teamScoreA, teamScoreB: teamScoreB)
                scores.append(score)
            }
        }
        return scores
    }
    
    static func parseStandingData(data: Data) -> [Standing] {
        let xml = XML.parse(data)
        var standings: [Standing] = []
        for hit in xml.gsmrs.competition.season.round.resultstable.ranking {
            let rank = Int(hit.attributes["rank"] ?? "1")
            let clubName = hit.attributes["club_name"]
            let matchedTotal = hit.attributes["matches_total"]
            let matchesWon = hit.attributes["matches_won"]
            let matchesDraw = hit.attributes["matches_draw"]
            let matchesLost = hit.attributes["matches_lost"]
            let totalGoals = hit.attributes["goals_pro"]
            let points = hit.attributes["points"]
            
            let stands = Standing(rank: rank, clubName: clubName, matchesPlayed: matchedTotal, matchesWon: matchesWon, matchesDraw: matchesDraw, matchesLost: matchesLost, totalGoals: totalGoals, points: points)
            standings.append(stands)
        }
        standings.sort(by: { $0.rank < $1.rank })
        return standings
    }
}
