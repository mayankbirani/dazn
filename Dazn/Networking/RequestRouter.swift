//
//  RequestRouter.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation
import Alamofire

let baseUrl = "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/"

enum RequestRouter {
    case news
    case score
    case standing
    case login(id: String, password: String)
    
    func values()->(url: URL, httpMethod: HTTPMethod, parameters:[String:Any]?,encoding: ParameterEncoding) {
        
        switch self {
        case .news:
            let params: [String: Any] = [:]
            let url = URL(string: "\(baseUrl)/latestnews.xml")!
            return (url: url, httpMethod: .get, parameters: params, encoding: URLEncoding.default)
            
        case .score:
            let params: [String: Any] = [:]
            let url = URL(string: "\(baseUrl)/scores.xml")!
            return (url: url, httpMethod: .get, parameters: params, encoding: URLEncoding.default)
            
        case .standing:
            let params: [String: Any] = [:]
            let url = URL(string: "\(baseUrl)/standings.xml")!
            return (url: url, httpMethod: .get, parameters: params, encoding: URLEncoding.default)
            
        case .login(let id, let password):
            let params: [String: Any] = ["loginId": id, "password": password]
            let url = URL(string: "\(baseUrl)/login")!
            return (url: url, httpMethod: .get, parameters: params, encoding: URLEncoding.default)
        }
    }
}
