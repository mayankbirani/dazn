//
//  HomeViewController.swift
//  Dazn
//
//  Created by Mayank Birani on 21/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

enum Section: Int {
    case news = 0, score, standing
}

class HomeViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var selectionViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var dropDownButton: UIBarButtonItem!
    
    var homeViewModel: HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeViewModel = HomeViewModel(homeDelegate: self)
        switchSections(tag: 0)
    }
    
    @IBAction func dropDownTapped(_ sender: UIBarButtonItem) {
        manageDropDown()
    }
    
    @IBAction func blurBackgroundTapped(_ sender: Any) {
        manageDropDown()
    }
    
    @IBAction func sectionSelected(_ sender: UIButton) {
        _ = self.children.map({
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        })
        self.blurView.alpha = 0
        selectionViewHeightConst.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
        switchSections(tag: sender.tag)
    }
    
    fileprivate func manageDropDown() {
        if selectionViewHeightConst.constant == 0 {
            blurView.alpha = 0.2
            selectionViewHeightConst.constant = 150
        } else {
            blurView.alpha = 0
            selectionViewHeightConst.constant = 0
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func switchSections(tag: Int) {
        let controller  = homeViewModel.pages[tag]
        addChild(controller)
        controller.view.frame = contentView.bounds
        contentView.addSubview(controller.view)
        
        if let sectionType = Section(rawValue: tag) {
            switch sectionType {
            case .news: self.title = "News"
            case .score: self.title = "Score"
            case .standing: self.title = "Standing"
            }
        }
    }
}

extension HomeViewController: HomeProtocol {}
