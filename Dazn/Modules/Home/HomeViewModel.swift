//
//  HomeViewModel.swift
//  Dazn
//
//  Created by Mayank Birani on 21/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

protocol HomeProtocol: class {}

class HomeViewModel {
    
    var homeDelegate: HomeProtocol
    
    lazy var pages: [UIViewController] = {
        return [
            getViewController(storyboardName: "News", withIdentifier: "NewsViewController"),
            getViewController(storyboardName: "Scores", withIdentifier: "ScoresViewController"),
            getViewController(storyboardName: "Standings", withIdentifier: "StandingsViewController")
        ]
    }()
    
    init(homeDelegate: HomeProtocol) {
        self.homeDelegate = homeDelegate
    }
}
