//
//  NewsDetailsViewController.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit
import WebKit

class NewsDetailsViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var newsDetailViewModel: NewsDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Article"
        if let url = newsDetailViewModel?.articleUrl {
            self.webView.navigationDelegate = self
            self.webView.load(URLRequest(url: url))
        }
    }
}

extension NewsDetailsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
        spinner.stopAnimating()
    }
}
