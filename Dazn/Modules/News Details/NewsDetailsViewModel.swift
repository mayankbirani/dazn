//
//  NewsDetailsViewModel.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation


class NewsDetailsViewModel {
    
    var articleUrl: URL?
    
    init(articleUrl: URL?) {
        self.articleUrl = articleUrl
    }
}
