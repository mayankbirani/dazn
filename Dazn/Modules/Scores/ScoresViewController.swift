//
//  ScoresViewController.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class ScoresViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var scores: [Score] = []
    var scoreViewModel: ScoresViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreViewModel = ScoresViewModel(delegate: self)
        scoreViewModel.getScores()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scoreViewModel.scoreRefreshEnable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        scoreViewModel.scoreRefreshEnable = false
    }
}

extension ScoresViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScoresTableCell") as? ScoresTableCell else {
            return UITableViewCell()
        }
        cell.formCell(score: scores[indexPath.row], isDark: indexPath.row % 2 == 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ScoresViewController: ScoresProtocol {
    func getScoreData(scores: [Score]) {
        spinner.stopAnimating()
        self.scores = scores
        self.tableView.reloadData()
    }
    
    func didGetError(error: String) {
        presentErrorAlert(message: error, view: self)
    }
}

