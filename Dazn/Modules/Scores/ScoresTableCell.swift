//
//  ScoresTableCell.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class ScoresTableCell: UITableViewCell {
    
    @IBOutlet weak var teamNameALabel: UILabel!
    @IBOutlet weak var teamNameBLabel: UILabel!
    @IBOutlet weak var teamScoreALabel: UILabel!
    @IBOutlet weak var teamScoreBLabel: UILabel!
    @IBOutlet weak var scoreView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func formCell(score: Score, isDark: Bool) {
        self.teamNameALabel.text = score.teamNameA
        self.teamNameBLabel.text = score.teamNameB
        self.teamScoreALabel.text = score.teamScoreA
        self.teamScoreBLabel.text = score.teamScoreB
        self.backgroundColor = isDark ? .grayCellBackgroundColor : .white
        addGradient(scoreView, [UIColor.grayGradientMaxColor.cgColor, UIColor.grayGradientMinColor.cgColor])
    }
}

