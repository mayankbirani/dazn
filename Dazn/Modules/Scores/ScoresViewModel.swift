//
//  ScoresViewModel.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

protocol ScoresProtocol: class {
    func getScoreData(scores: [Score])
    func didGetError(error: String)
}

class ScoresViewModel {
    
    var scoreDelegate: ScoresProtocol
    var scoreTimer: Timer?
    var scoreRefreshEnable: Bool = false {
        didSet {
            scoreRefreshEnable == true ? reloadScores() : scoreTimer?.invalidate()
        }
    }
    
    init(delegate: ScoresProtocol) {
        scoreDelegate = delegate
    }
    
    @objc func getScores() {
        DataService.getScoreData(request: .score, successHandler: {(scores) in
            self.scoreDelegate.getScoreData(scores: scores)
        }) { (error) in
            self.scoreDelegate.didGetError(error: error)
        }
    }
    
    fileprivate func reloadScores() {
        scoreTimer = Timer.scheduledTimer(timeInterval: scoreReloadTimeInSeconds, target: self, selector: #selector(getScores), userInfo: nil, repeats: true)
    }
}
