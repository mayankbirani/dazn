//
//  Score.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct Score {
    var date: String
    var teamNameA: String
    var teamNameB: String
    var teamScoreA: String
    var teamScoreB: String
    
    init(date: String?, teamNameA: String?, teamNameB: String?, teamScoreA: String?, teamScoreB: String?) {
        self.date = date ?? ""
        self.teamNameA = teamNameA ?? ""
        self.teamNameB = teamNameB ?? ""
        self.teamScoreA = teamScoreA ?? ""
        self.teamScoreB = teamScoreB ?? ""
    }
}
