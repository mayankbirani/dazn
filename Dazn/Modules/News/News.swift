//
//  News.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct News {
    var title: String
    var description: String
    var pubDate: String
    var imageUrl: URL?
    var itemLink: URL?
    
    init(title: String?, description: String?, pubDate: String?, imageUrl: String?, itemLink: String?) {
        self.title = title ?? ""
        self.description = description ?? ""
        self.pubDate = pubDate ?? ""
        self.imageUrl = URL(string: imageUrl ?? "")
        self.itemLink = URL(string: itemLink ?? "")
    }
}
