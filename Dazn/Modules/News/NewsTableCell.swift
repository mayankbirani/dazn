//
//  NewsTableCell.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsTableCell: UITableViewCell {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsPubDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func formCell(news: News) {
        self.newsTitle.text = news.title
        self.newsDescription.text = news.description
        self.newsPubDate.text = news.pubDate
        if let url = news.imageUrl {
            self.newsImageView.af_setImage(withURL: url)
        }
    }
}
