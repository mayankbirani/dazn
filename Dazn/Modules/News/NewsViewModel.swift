//
//  NewsViewModel.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

protocol NewsProtocol: class {
    func getNewsData(news: [News])
    func didGetError(error: String)
}

class NewsViewModel {
    
    var newsDelegate: NewsProtocol
    
    init(delegate: NewsProtocol) {
        newsDelegate = delegate
    }
    
    func getNews() {
        DataService.getNewsData(request: .news, successHandler: {(news) in
            self.newsDelegate.getNewsData(news: news)
        }) { (error) in
            self.newsDelegate.didGetError(error: error)
        }
    }
    
    func showArticle(articleUrl: URL?, view: UIViewController) {
        
        if let controller = getViewController(storyboardName: "NewsDetails", withIdentifier: "NewsDetailsViewController") as? NewsDetailsViewController {
            let newsDetailVM = NewsDetailsViewModel(articleUrl: articleUrl)
            controller.newsDetailViewModel = newsDetailVM
            view.show(controller, sender: nil)
        }
    }
}
