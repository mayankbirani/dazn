//
//  NewsViewController.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var newsList: [News] = []
    var newsViewModel: NewsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsViewModel = NewsViewModel(delegate: self)
        newsViewModel.getNews()
    }
}

extension NewsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableCell") as? NewsTableCell else {
            return UITableViewCell()
        }
        cell.formCell(news: newsList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        newsViewModel.showArticle(articleUrl: newsList[indexPath.row].itemLink, view: self)
    }
}

extension NewsViewController: NewsProtocol {
    func getNewsData(news: [News]) {
        spinner.stopAnimating()
        self.newsList = news
        self.tableView.reloadData()
    }
    
    func didGetError(error: String) {
        presentErrorAlert(message: error, view: self)
    }
}


