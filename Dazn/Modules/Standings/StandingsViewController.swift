//
//  StandingsViewController.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class StandingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var standing: [Standing] = []
    var standingViewModel: StandingsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        standingViewModel = StandingsViewModel(delegate: self)
        standingViewModel.getStandings()
    }
}

extension StandingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return standing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StandingTableViewCell") as? StandingTableViewCell else {
            return UITableViewCell()
        }
        cell.formCell(standing: standing[indexPath.row], isDark: indexPath.row % 2 == 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StandingSection") else {
            return nil
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
}

extension StandingsViewController: StandingsProtocol {
    func getStandingData(standing: [Standing]) {
        spinner.stopAnimating()
        self.standing = standing
        tableView.reloadData()
    }
    
    func didGetError(error: String) {
        presentErrorAlert(message: error, view: self)
    }
}



