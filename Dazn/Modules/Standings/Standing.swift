//
//  Standing.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct Standing {
    var rank: Int
    var clubName: String
    var matchesPlayed: String
    var matchesWon: String
    var matchesDraw: String
    var matchesLost: String
    var totalGoals: String
    var points: String
    
    init(rank: Int?, clubName: String?, matchesPlayed: String?, matchesWon: String?, matchesDraw: String?, matchesLost: String?, totalGoals: String?, points: String?) {
        self.rank = rank ?? 1
        self.clubName = clubName ?? ""
        self.matchesPlayed = matchesPlayed ?? ""
        self.matchesWon = matchesWon ?? ""
        self.matchesDraw = matchesDraw ?? ""
        self.matchesLost = matchesLost ?? ""
        self.totalGoals = totalGoals ?? ""
        self.points = points ?? ""
    }
}
