//
//  StandingsViewModel.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

protocol StandingsProtocol: class {
    func getStandingData(standing: [Standing])
    func didGetError(error: String)
}

class StandingsViewModel {
    
    var standingDelegate: StandingsProtocol
    
    init(delegate: StandingsProtocol) {
        standingDelegate = delegate
    }
    
    func getStandings() {

        DataService.getStandingsData(request: .standing, successHandler: { (standings) in
            self.standingDelegate.getStandingData(standing: standings)
        }) { (error) in
            self.standingDelegate.didGetError(error: error)
        }
    }
}
