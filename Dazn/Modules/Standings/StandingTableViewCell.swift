//
//  StandingTableViewCell.swift
//  Dazn
//
//  Created by Mayank Birani on 26/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class StandingTableViewCell: UITableViewCell {
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var clubNameLabel: UILabel!
    @IBOutlet weak var playedLabel: UILabel!
    @IBOutlet weak var wonLabel: UILabel!
    @IBOutlet weak var lostLabel: UILabel!
    @IBOutlet weak var drawLabel: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func formCell(standing: Standing, isDark: Bool) {
        self.rankLabel.text = String(standing.rank)
        self.clubNameLabel.text = standing.clubName
        self.playedLabel.text = standing.matchesPlayed
        self.wonLabel.text = standing.matchesWon
        self.lostLabel.text = standing.matchesLost
        self.drawLabel.text = standing.matchesDraw
        self.goalsLabel.text = standing.totalGoals
        self.pointsLabel.text = standing.points
        let cellColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        self.backgroundColor = isDark ? cellColor : .white
    }
}
